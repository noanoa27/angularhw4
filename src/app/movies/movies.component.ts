import { Component, OnInit } from '@angular/core';

import {AngularFireDatabase,AngularFireList} from '@angular/fire/database';

@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})



export class MoviesComponent implements OnInit {

  name= "no movie";

  movies = [ ];

 displayedColumns: string[] = ['id', 'title', 'studio', 'weekendIncome','delete'];

 toDelete(element)
 {
   let x,y = this.movies;
   let z = element.id;

   if (this.movies.length == 1)
   {
     this.movies = [];
   }
   if (z > this.movies.length)
   {
     z = this.movies.length-1;
   }
   
   x = this.movies.slice(0,z-1);
   y = this.movies.slice(z, this.movies.length);
   this.movies = x;
   this.movies = this.movies.concat(y);
   this.name = element.title;
  
 }

  constructor(private db:AngularFireDatabase) { }

  ngOnInit() {
    this.db.list('/movies').snapshotChanges().subscribe(
          movies =>{
              this.movies=[];
              movies.forEach(
               movie =>{
                 let y = movie.payload.toJSON();
                 // y["$key"] = movie.key;
                  this.movies.push(y);
               }
              )
            }
         )
  }

}

